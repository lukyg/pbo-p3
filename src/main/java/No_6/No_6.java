package No_6;

public class No_6 {
    String nama;
    String nim;
    Double kelompok;
    Integer tanggal;
    No_6(){
        nim = "F1B019005";
    }
    No_6(String nama){
        this.nama = nama;
    }
    No_6(Double klp){
        kelompok = klp;
    }
    No_6(Integer tgl){
        this.tanggal = tgl;
    }
    public static void main(String[] args) {
        No_6 J61 = new No_6();
        No_6 J62 = new No_6("Ade Safarudin Madani");
        No_6 J63 = new No_6(1);
        No_6 J64 = new No_6(5.0);
        consolelog("Nama           : " + J62.nama);
        consolelog("NIM            : " + J61.nim);
        consolelog("Kelompok       : " + J64.kelompok);
        consolelog("Tanggal Lahir  : " + J63.tanggal);
        No_6 J65 = new No_6("Kucing");
        No_6 J66 = new No_6(5);
        consolelog("Nama Hewan kesayangan : " + J65.nama);
        consolelog("NIM                   : " + J61.nim);
        consolelog("Kelompok              : " + J64.kelompok);
        consolelog("Bulan Lahir           : " + J66.tanggal);
    }
    static void consolelog(String x){
        System.out.println(x);
    }
}
