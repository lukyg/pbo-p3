package No_3;

class No_3 {
    String nama;
    String nim;
    String kelompok;

    No_3(){

    }

    No_3(String nama, String nim){
        this.nama = nama;
        this.nim = nim;
    }

    No_3(String kelompok){
        this.kelompok = kelompok;
    }

    public static void main(String[] args) {
        No_3 coba = new No_3("Ade Safarudin Madani","F1B019005");
        No_3 coba1 = new No_3("5");

        System.out.println("nama : " + coba.nama);
        System.out.println("NIM : "+coba.nim);
        System.out.println("Kelompok : "+coba1.kelompok);
    }
}
