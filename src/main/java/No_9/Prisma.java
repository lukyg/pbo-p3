package No_9;

import java.util.Scanner;

public class Prisma {
    Integer x, y, z;
    Integer hasil;
    Prisma(){
        System.out.println("Tidak ada nilai yang dapat di hitung !!!!!");
    }
    ///segi empat
    Prisma(Integer panjang, Integer lebar, Integer tinggi){
        x = panjang;
        y = lebar;
        z = tinggi;
        hasil = 2 * ((x*y) + (x*z) + (y*z));
    }
    ////segi 6
    Prisma(int lAlas, int lSelimut){
        x = lAlas;
        y = lSelimut;
        hasil = 2 * x + y;
    }
    void cetak(){
        System.out.println("Hasil Perhitungan luas prisma : " + hasil);
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Prisma prs = new Prisma();
        System.out.print("Masukkan nilai panjang    : ");
        int pjg = input.nextInt();
        System.out.print("Masukkan nilai lebar      : ");
        int lbr = input.nextInt();
        System.out.print("Masukkan nilai tinggi     : ");
        int tinggi = input.nextInt();
        Prisma prs2 = new Prisma(pjg,lbr,tinggi);
        System.out.println("=======");
        System.out.print("Masukkan nilai luas alas      : ");
        int lAlas = input.nextInt();
        System.out.print("Masukkan nilai luas selimut   : ");
        int lSelimut = input.nextInt();
        Prisma prs3 = new Prisma(lAlas,lSelimut);
        prs2.cetak();
        System.out.println("===========");
        prs3.cetak();
    }
}

