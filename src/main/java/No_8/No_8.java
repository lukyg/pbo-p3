package No_8;

public class No_8 {
    double phi;
    double jari_jari;
    double selimut;
    double hasil;
    No_8(){
        phi = 3.14;
    }
    No_8(double r, double s){
        phi = 3.14;
        hasil = phi * r * (r + s);
    }
    No_8(double r){
        jari_jari = r;
    }
    No_8( String x, double s){
        System.out.println(x + s);
    }
    public void setJari_jari(double jari_jari) {
        this.jari_jari = jari_jari;
    }
    public void setSelimut(double selimut) {
        this.selimut = selimut;
    }
    public double getJari_jari() {
        return jari_jari;
    }
    public double getSelimut() {
        return selimut;
    }
    double hitung(double r, double s){
        hasil = phi * r * (r + s);
        return hasil;
    }

    public static void main (String[]args){
        No_8 J8 = new No_8();
        System.out.println("Nilai phi kerucut           : " + J8.phi);
        No_8 J81 = new No_8(9,1);
        System.out.println("Nilai perhitungan kerucut2  : " + J81.hasil);
        No_8 J82 = new No_8();
        J82.setJari_jari(3);
        J82.setSelimut(5);
        double hasil = J8.hitung(J82.getJari_jari(),
                J82.getSelimut());
        System.out.println("Hasil kerucut 3             : " + hasil);
        No_8 J83 = new No_8(3);
        System.out.println("Print jari kerucut4         : "+
                J83.jari_jari);
        No_8 J84 = new No_8("Selimut kerucut5            : ", 3);

    }
}
