package No_5;

public class No_5 {
    String nama;
    String nim;
    String kelompok;
    String ttl;

    No_5(){
        nama = "Ade Safarudin Madani";
        nim = "F1B019005";
        kelompok = "5";
        ttl = "1";
    }

    No_5(String nama, String ttl){
        this.nama = nama;
        nim = "F1B019005";
        kelompok = "5";
        this.ttl = ttl;
    }

    public static void main(String[] args) {
        No_5 coba1 = new No_5();
        No_5 coba2 = new No_5("Kasur California","5");

        System.out.println("nama : "+coba1.nama);
        System.out.println("nim : "+coba1.nim);
        System.out.println("kelompok : "+coba1.kelompok);
        System.out.println("tanggal : "+coba1.ttl);

        System.out.println("\nHasil setelah diubah\n");
        System.out.println("nama : "+coba2.nama);
        System.out.println("nim : "+coba2.nim);
        System.out.println("kelompok : "+coba2.kelompok);
        System.out.println("tanggal : "+coba2.ttl);
    }
}
