package No_4;

public class No_4 {
    int umur;
    int angkatan;
    int tanggalKerja;

    No_4(){
        umur = 20;
    }

    No_4(int angkatan, int tanggalKerja){
        this.angkatan = angkatan;
        this.tanggalKerja = tanggalKerja;
    }

    public static void main(String[] args) {
        No_4 testing = new No_4(19, 19);
        System.out.println("Hasil tambah angkatan : "+(testing.angkatan+2));
        System.out.println("Hasil kali tanggal kerja : "+testing.tanggalKerja*2);

    }
}
