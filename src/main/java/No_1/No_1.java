package No_1;

import java.util.Scanner;

class No_1 {

    private String nama;
    private String nim;
    private String kelompok;

    void No_1(){
        System.out.println(nama);
        System.out.println(nim);
        System.out.println(kelompok);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        No_1 coba = new No_1();
        System.out.print("Masukkan nama : ");
        coba.nama = input.nextLine();
        System.out.print("Masukkan nim : ");
        coba.nim = input.nextLine();
        System.out.print("Masukkan kelompok : ");
        coba.kelompok = input.nextLine();

        System.out.println("Nama saya adalah "+coba.nama);
        System.out.println("NIM saya adalah "+coba.nim);
        System.out.println("Saya anggota kelompok "+coba.kelompok);
    }
}
