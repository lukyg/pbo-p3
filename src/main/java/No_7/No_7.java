package No_7;


public class No_7 {
    Integer p;
    Integer l;
    Integer t;
    Integer hasil;
    Double hasil2;
    Double pjg, lbr, tg;

    No_7() {
        p = 2;
        l = 3;
        t = 4;
    }

    No_7(Integer panjang, Integer lebar, Integer tinggi){
        p = panjang;
        l = lebar;
        t = tinggi;
        hasil = p * l * t;
    }

    No_7(Double panjang, Double lebar, Double tinggi){
        pjg = panjang;
        lbr = lebar;
        tg = tinggi;
        hasil2 = pjg * lbr * tg;
    }

    Integer hitung () {
        hasil = p * l * t;
        return hasil;
    }

    public static void main (String[]args){
        No_7 J7 = new No_7();
        System.out.println("Hasil perhitungan objek 1 : " + J7.hitung());
        No_7 J71 = new No_7(6, 7, 8);
        System.out.println("Hasil Perhitungan objek 2 : " +
                J71.hasil);
        No_7 J72 = new No_7(3.2, 6.3, 9.4);
        System.out.println("Hasil Perhitungan objek 2 : " +
                J72.hasil2);
    }
}

