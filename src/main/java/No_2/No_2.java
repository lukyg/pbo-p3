package No_2;

import java.util.Scanner;

class No_2 {
    private String nama;
    private String nim;
    private String kelompok;

    void No_2 (String nama, String nim, String kelompok){
        this.nama = nama;
        this.nim = nim;
        this.kelompok = kelompok;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        No_2 cobalagi = new No_2();

        cobalagi.nama = input.nextLine();
        cobalagi.nim = input.nextLine();
        cobalagi.kelompok = input.nextLine();

        System.out.println("Nama saya adalah "+cobalagi.nama);
        System.out.println("NIM saya adalah "+cobalagi.nim);
        System.out.println("Saya anggota kelompok "+cobalagi.kelompok);
    }
}
